/* eslint-disable no-console */
import dotenv from 'dotenv';
import { getWebpackCompiler } from './compiler.js';

// require('dotenv').config();
// const chalk = require('chalk');

dotenv.config();

console.log('START BUILD');

const webpackCompiler = getWebpackCompiler();
const infoOptions = {
  hash: true,
  colors: true,
  env: true,
  logging: 'info',
  modules: false,
};

const showStats = (stats) => {
  const info = stats.toString(infoOptions);

  console.log(info);

  if (stats.hasErrors()) {
    console.error(info.errors);
  }

  if (stats.hasWarnings()) {
    console.warn(info.warnings);
  }
};

webpackCompiler.run((err, stats) => {
  if (err) {
    console.error(err.stack || err);
    if (err.details) {
      console.error(err.details);
    }
    return;
  }

  showStats(stats);
});
