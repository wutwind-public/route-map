import { merge } from 'webpack-merge';

import * as loaders from '../modules/loaders/index.js';
import * as plugins from '../modules/plugins/index.js';
import * as optimize from '../modules/optimization/index.js';

export const getWebpackProdConfig = () => merge(
  {
    mode: 'production',
  },
  // loaders
  loaders.cssLoaderProd(),
  loaders.sassLoaderProd(),
  // plugins
  plugins.miniCssExtractPlugin(),
  plugins.bundleAnalyzePlugin(),
  // optimize
  optimize.buildOptimize(),
  optimize.filterMomentLocale(),
);
