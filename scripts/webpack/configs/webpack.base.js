import appRootPath from 'app-root-path';

import * as loaders from '../modules/loaders/index.js';
import * as plugins from '../modules/plugins/index.js';
import { merge } from 'webpack-merge';

import getWebpackRootConfig from '../../../webpack.base.config.cjs';

function resolve(dir) {
  return appRootPath.resolve(dir);
}

// const getWebpackRootConfig = require(resolve('webpack.base.config.js'));

export const getWebpackBaseConfig = () => merge(
  {
    entry: {
      app: resolve('src/main.js'),
    },
    output: {
      clean: true,
      path: resolve('dist'),
      publicPath: '/',
      assetModuleFilename: 'assets/[hash][ext]',
      filename: 'assets/js/[name].[contenthash:8].js',
      chunkFilename: 'assets/js/[name].[chunkhash:8].js',
    },
    module: {
      noParse: /^(vue|vue-router|vuex|vuex-router-sync)$/u,
    },
    resolve: getWebpackRootConfig().resolve,
    stats: {
      hash: true,
      colors: true,
      env: true,
      logging: 'info',
      modules: false,
    },
  },
  // loaders
  loaders.vueLoader(),
  loaders.jsLoader(),
  loaders.imagesLoader(),
  loaders.fontsLoader(),
  // plugins
  plugins.vuePlugin(),
  plugins.htmlPlugin(),
  plugins.definePlugin(),
  plugins.progressBarPlugin(),
);
