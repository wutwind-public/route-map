import { merge } from 'webpack-merge';
import * as loaders from '../modules/loaders/index.js';
import * as plugins from '../modules/plugins/index.js';

export const getWebpackDevConfig = () => merge(
  {
    entry: {
      app: './src/main.js',
    },
    mode: 'development',
    devtool: 'eval-cheap-module-source-map',
  },
  loaders.cssLoaderDev(),
  loaders.sassLoaderDev(),
  plugins.hotReplacePlugin(),
);
