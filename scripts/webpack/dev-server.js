import dotenv from 'dotenv';
import WebpackDevServer from 'webpack-dev-server';
import { getWebpackCompiler } from './compiler.js';

dotenv.config();

const webpackCompiler = getWebpackCompiler();
const devServerOptions = {
  compress: true,
  hot: true,
  historyApiFallback: true,
  allowedHosts: 'all',
  port: 80,
  client: {
    webSocketTransport: 'sockjs',
  },
  webSocketServer: 'sockjs',
};

const server = new WebpackDevServer(devServerOptions, webpackCompiler);
server.start();
