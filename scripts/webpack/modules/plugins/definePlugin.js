import process from 'node:process';
import webpack from 'webpack';

const { DefinePlugin } = webpack;

export const definePlugin = () => ({
  plugins: [
    new DefinePlugin({
      __ENV__: JSON.stringify(process.env.NODE_ENV),
    }),
  ],
});
