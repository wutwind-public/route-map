import HtmlWebpackPlugin from 'html-webpack-plugin';

export const htmlPlugin = () => ({
  plugins: [
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: 'public/index.html',
      favicon: 'public/favicon.svg',
      inject: true,
    }),
  ],
});
