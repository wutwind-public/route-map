import webpack from 'webpack';

const { HotModuleReplacementPlugin } = webpack;
export const hotReplacePlugin = () => ({
  plugins: [
    new HotModuleReplacementPlugin(),
  ],
});
