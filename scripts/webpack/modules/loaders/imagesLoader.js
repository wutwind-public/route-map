export const imagesLoader = () => ({
  module: {
    rules: [
      {
        test: /\.(png|svg|jpg|jpeg|gif)$/ui,
        type: 'asset/resource',
        generator: {
          filename: 'assets/images/[name].[hash:8][ext][query]',
        },
      },
    ],
  },
});
