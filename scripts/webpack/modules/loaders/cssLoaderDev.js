export const cssLoaderDev = () => ({
  module: {
    rules: [
      {
        test: /\.css$/u,
        use: [
          'vue-style-loader',
          'css-loader',
        ],
      },
    ],
  },
});
