export const sassLoaderDev = () => ({
  module: {
    rules: [
      {
        test: /\.s[ca]ss$/ui,
        use: [
          'vue-style-loader',
          'css-loader',
          'sass-loader',
        ],
      },
    ],
  },
});
