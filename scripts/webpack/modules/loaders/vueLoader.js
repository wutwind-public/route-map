export const vueLoader = () => ({
  module: {
    rules: [
      {
        test: /\.vue$/u,
        loader: 'vue-loader',
      },
    ],
  },
});
