export const jsLoader = () => ({
  module: {
    rules: [
      {
        test: /\.js$/u,
        loader: 'babel-loader',
        exclude: /node_modules/u,
      },
    ],
  },
});
