export { jsLoader } from './jsLoader.js';
export { vueLoader } from './vueLoader.js';

export { fontsLoader } from './fontsLoader.js';
export { imagesLoader } from './imagesLoader.js';

export { cssLoaderDev } from './cssLoaderDev.js';
export { cssLoaderProd } from './cssLoaderProd.js';
export { sassLoaderDev } from './sassLoaderDev.js';
export { sassLoaderProd } from './sassLoaderProd.js';
