// this plugin can filter libraries and minimize bundle
import webpack from 'webpack';

const { ContextReplacementPlugin } = webpack;

// test build optimization
export const filterMomentLocale = () => ({
  plugins: [
    new ContextReplacementPlugin(
      /moment\/locale/u,
      /ru|en/u,
    ),
  ],
});
