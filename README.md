# RouteMap

## LDE commands

- `lde build` - build docker images
- `lde bush` - run node container with bash
- `lde up` - up all app containers for local development
- `lde down` - down all app containers
