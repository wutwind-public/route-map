import path, { dirname } from 'node:path';
import { fileURLToPath } from 'node:url';

const currentDir = dirname(fileURLToPath(import.meta.url));
const concat = (...arrays) => [].concat(...arrays);

export default {
  extends: concat(
    [
      'stylelint-config-standard',
      'stylelint-config-standard-scss',
      'stylelint-config-standard-vue/scss',
    ],
    [
      './tools/stylelint-config/base.js',
      './tools/stylelint-config/stylistic.js',
      './tools/stylelint-config/order.js',
      './tools/stylelint-config/conflicts.js',
      './tools/stylelint-config/scss.js',
    ].map((string) => path.resolve(currentDir, string)),
  ),
  plugins: [
    '@stylistic/stylelint-plugin',
  ],
};
