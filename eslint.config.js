import {
  progress, nodeConfig, jsConfig, vueConfig,
} from './tools/eslint-config/index.js';

export default [
  progress,
  nodeConfig,
  jsConfig,
  vueConfig,
];
