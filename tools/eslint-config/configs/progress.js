import fileProgressPlugin from 'eslint-plugin-file-progress';

export default {
  plugins: {
    'file-progress': fileProgressPlugin,
  },
  rules: {
    'file-progress/activate': 1,
  },
  settings: {
    progress: {
      hide: false, successMessage: 'Lint done! Success!',
    },
  },
};
