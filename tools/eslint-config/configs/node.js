import { jsRules } from '../rules/javascript/index.js';

export default {
  files: ['**/*.cjs'],
  languageOptions: {
    sourceType: 'commonjs',
    parserOptions: {
      sourceType: 'commonjs',
    },
  },
  rules: {
    ...jsRules,
  },
};
