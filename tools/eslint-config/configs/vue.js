import { vueRules } from '../rules/vue/index.js';
import { jsRules } from '../rules/javascript/index.js';

import { jsFormatting } from '../rules/stylistic/index.js';

import vuePlugin from 'eslint-plugin-vue';
import stylisticPlugin from '@stylistic/eslint-plugin';

import vueParser from 'vue-eslint-parser';
import globals from 'globals';

export default {
  files: ['**/*.vue', '**/*.ts'],
  plugins: {
    'vue': vuePlugin,
    '@stylistic': stylisticPlugin,
  },
  languageOptions: {
    parser: vueParser,
    parserOptions: {
      project: true,
      extraFileExtensions: ['.vue'],
    },
    globals: {
      ...globals.browser,
    },
  },
  rules: {
    ...jsRules,
    ...jsFormatting,
    ...vueRules,
  },
};
