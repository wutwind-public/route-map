export { default as jsConfig } from './configs/javascript.js';
export { default as nodeConfig } from './configs/node.js';
export { default as vueConfig } from './configs/vue.js';

export { default as progress } from './configs/progress.js';
