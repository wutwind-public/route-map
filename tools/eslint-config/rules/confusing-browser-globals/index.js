import browsersGlobal from './confusingBrowsersGlobals.js';

const additionalGlobals = [
  {
    name: 'isFinite',
    message: 'Use Number.isFinite instead https://github.com/airbnb/javascript#standard-library--isfinite',
  },
  {
    name: 'isNaN',
    message: 'Use Number.isNaN instead https://github.com/airbnb/javascript#standard-library--isnan',
  },
];

export default [
  ...browsersGlobal,
  ...additionalGlobals,
];
