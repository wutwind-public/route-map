export const INDENT_SIZE = 2;
export const STR_LENGTH = 100;

export const MAX_LEN = {
  code: STR_LENGTH,
  tabWidth: INDENT_SIZE,

  ignoreComments: false,
  ignoreTrailingComments: false,

  ignoreUrls: true,
  ignoreStrings: true,
  ignoreTemplateLiterals: true,
  ignoreRegExpLiterals: true,
};
