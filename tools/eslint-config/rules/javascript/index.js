import { possibleProblemRules } from './possible-problems.js';
import { suggestionRules } from './suggestions.js';

export const jsRules = {
  ...possibleProblemRules,
  ...suggestionRules,
};
