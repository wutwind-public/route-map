/* eslint
    sort-keys: ["error", "asc", {
        "minKeys": 5,
        "allowLineSeparatedGroups": true
    }]
*/

import { ERROR, OFF } from '../severity.js';
import { INDENT_SIZE } from '../constants.js';

export const formattingRules = {
  'vue/block-tag-newline': [ERROR, {
    singleline: 'always',
    multiline: 'always',
    maxEmptyLines: 0,
  }],
  'vue/define-macros-order': [ERROR, {
    order: ['defineProps', 'defineEmits'],
  }],
  'vue/first-attribute-linebreak': [ERROR, {
    singleline: 'beside',
    multiline: 'below',
  }],
  'vue/html-closing-bracket-newline': [ERROR],
  'vue/html-closing-bracket-spacing': [ERROR],
  'vue/html-comment-content-newline': [ERROR],
  'vue/html-comment-content-spacing': [ERROR],
  'vue/html-comment-indent': [ERROR, INDENT_SIZE],
  'vue/html-indent': [ERROR, INDENT_SIZE],
  'vue/html-quotes': [ERROR, 'double'],
  'vue/html-self-closing': [ERROR],
  'vue/max-attributes-per-line': [ERROR, {
    singleline: { max: 2 },
    multiline: { max: 1 },
  }],
  'vue/multiline-html-element-content-newline': [ERROR],
  'vue/mustache-interpolation-spacing': [ERROR, 'always'],
  'vue/new-line-between-multi-line-property': [OFF],
  'vue/no-multi-spaces': [ERROR],
  'vue/no-spaces-around-equal-signs-in-attribute': [ERROR],
  'vue/padding-line-between-blocks': [ERROR, 'always'],
  'vue/padding-line-between-tags': [OFF],
  'vue/padding-lines-in-component-definition': [OFF],
  'vue/script-indent': [OFF],
  'vue/singleline-html-element-content-newline': [OFF],
  'vue/v-for-delimiter-style': [ERROR, 'in'],
};
