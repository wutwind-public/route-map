import { ERROR, OFF } from '../severity.js';

export const baseRules = {
  'vue/comment-directive': [OFF],
  'vue/jsx-uses-vars': [ERROR],
};
