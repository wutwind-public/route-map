/* eslint
    sort-keys: ["error", "asc", {
        "minKeys": 4,
        "allowLineSeparatedGroups": true
}] */

export default {
  rules: {
    'color-hex-alpha': 'never',
    'color-hex-length': 'long',
    'color-named': 'never',
    'declaration-no-important': true,
    'declaration-property-value-no-unknown': true,
    'font-weight-notation': ['numeric', {
      ignore: ['relative'],
    }],
    'max-nesting-depth': [4, {
      ignore: [
        'blockless-at-rules',
        'pseudo-classes',
      ],
    }],
    'media-feature-name-value-no-unknown': true,
    'selector-max-compound-selectors': 3,
    'selector-max-id': 0,
    'selector-max-universal': 2,
  },
};
