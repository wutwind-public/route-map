/* eslint
    sort-keys: ["error", "asc", {
        "minKeys": 4,
        "allowLineSeparatedGroups": true
}] */

export default {
  rules: {
    /* $-variables */
    'scss/dollar-variable-empty-line-before': ['never', { ignore: 'after-dollar-variable' }],
  },
};
